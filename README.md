Hello! This is a horror puzzle game called Ligati. 
This is a PC 2d platformer written in python using the pygame library.
We are currently in an early development stage.

Our current work is in 3 branches:
	ConfigReadWriter, LightEngine, and player_prototype.
No work has been merged into master yet.

If you have any questions or comments, 
you can contact the project lead Enoch Tsang at etsang1@hotmail.com.

Here are our teams and their respective branches:
Team Gold - LightEngine
	Sven Zhang - Team Lead
	Enoch Tsang - Project Lead
	Bea Esguerra
	Adnan Husain

Team Awesome - ConfigReadWriter
	Byron Seto - Team Lead
	Sam Ao
	Tai Lai
	Henry Tran - Sneaky Turtle

Team Royal - player_prototype
	Spencer Walton - Team Lead
	William Ling