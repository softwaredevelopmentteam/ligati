import sys
import pygame

pygame.init()

SCREEN = width, height = 300, 300
WHITE = 255, 255, 255
RED = 255, 0, 0
hitbox = pygame.Rect(125, 125, 50, 50)

screen = pygame.display.set_mode(SCREEN)

while 1:
	for event in pygame.event.get():
		if event.type == pygame.QUIT: sys.exit()

	screen.fill(WHITE)

	if hitbox.collidepoint(pygame.mouse.get_pos()):
		if pygame.mouse.get_pressed()[0]:
			sys.exit()
		pygame.draw.rect(screen, RED, hitbox)
	else:
		pygame.draw.rect(screen, RED, hitbox, 2)

	pygame.display.flip()


